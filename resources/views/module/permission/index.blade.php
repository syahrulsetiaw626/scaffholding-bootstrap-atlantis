<x-base-app-layout>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1 class="display-5 fw-bold">Management Permission</h1>
                <p>Halaman untuk management permission</p>
            </div>
            <div class="col-12 mb-2 mb-md-4">
                <form action="{{ route('permission.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="name" placeholder="Input New Permission">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary" id="basic-addon2">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 p-0">
                <div class="table-responsive">
                    <table class="table" id="tablePermission">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($permission as $p => $permi)
                            <tr>
                                <td>{{ $p+1 }}</td>
                                <td>{{ $permi->name }}</td>
                                <td>
                                    <span class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Edit</span>
                                    <form action="{{ route('permission.destroy', $permi->id) }}" method="POST" class="d-inline ms-2">
                                        @csrf 
                                        @method('delete')
                                        <button type="submit" class="btn btn-primary" onclick="if( !confirm('Do you want to continue?') ) return false;">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Permission Update</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" id="formUpdate">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Input New Permission">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary"
                                                id="basic-addon2">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="script">
        <script>
            $("#tablePermission").DataTable();
        </script>
    </x-slot>
</x-base-app-layout>
