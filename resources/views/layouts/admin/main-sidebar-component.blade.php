<!-- Sidebar -->
<div class="sidebar sidebar-style-2" data-background-color="dark2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            
            <x-user-sidebar-component></x-user-sidebar-component>
            
            <x-main-nav-sidebar-component></x-main-nav-sidebar-component>
        </div>
    </div>
</div>
<!-- End Sidebar -->
