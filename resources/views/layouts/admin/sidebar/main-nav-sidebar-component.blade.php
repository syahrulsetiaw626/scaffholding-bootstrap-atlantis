<ul class="nav nav-primary">

    <x-single-list-sidebar-component link="" activate="">
        <x-slot name="icon"><i class="la flaticon-layers-1"></i></x-slot>
        <x-slot name="text">Dashboard</x-slot>
    </x-single-list-sidebar-component>

    <li class="nav-section">
        <span class="sidebar-mini-icon">
            <i class="fa fa-ellipsis-h"></i>
        </span>
        <h4 class="text-section">Components</h4>
    </li>

    {{-- ini contoh sidebar dropdown --}}
    <x-multiple-list-sidebar-component idSidebar="chart">
        <x-slot name="icon"><i class="la flaticon-interface-1"></i></x-slot>
        <x-slot name="text">User</x-slot>
        {{-- ini contoh untuk sub sidebarnya, link dan activate silahkan ganti sesuai penggunaan --}}
        <x-sublist-multiple-sidebar-component link="/index.html" activate="sublist">
            <x-slot name="text">list</x-slot>
        </x-sublist-multiple-sidebar-component>
    </x-multiple-list-sidebar-component>

    {{-- ini contoh single sidebar --}}
    <x-single-list-sidebar-component link="/index.html" activate="index">
        <x-slot name="icon"><i class="la flaticon-interface-1"></i></x-slot>
        <x-slot name="text">User</x-slot>
        <x-slot name="notif">Hallo</x-slot>
    </x-single-list-sidebar-component>

    <li class="nav-section">
        <span class="sidebar-mini-icon">
            <i class="fa fa-ellipsis-h"></i>
        </span>
        <h4 class="text-section">User & Roles</h4>
    </li>

    {{-- ini contoh single sidebar --}}
    <x-single-list-sidebar-component link="{{ route('roles.index') }}" activate="roles">
        <x-slot name="icon"><i class="la flaticon-user-5"></i></x-slot>
        <x-slot name="text">Roles</x-slot>
    </x-single-list-sidebar-component>
    <x-single-list-sidebar-component link="{{ route('permission.index') }}" activate="permission">
        <x-slot name="icon"><i class="la flaticon-lock-1"></i></x-slot>
        <x-slot name="text">Permission</x-slot>
    </x-single-list-sidebar-component>


</ul>
