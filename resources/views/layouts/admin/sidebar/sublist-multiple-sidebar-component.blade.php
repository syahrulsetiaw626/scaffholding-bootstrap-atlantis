<li @if(Request::segment(2) == $activate)
    class="active"
@endif>
    <a href="{{ $link }}">
        <span class="sub-item">{{ $text }}</span>
    </a>
</li>


