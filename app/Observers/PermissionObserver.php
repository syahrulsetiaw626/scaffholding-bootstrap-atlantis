<?php

namespace App\Observers;

use App\Helper\LogActivity;
use Spatie\Permission\Models\Permission;

class PermissionObserver
{
    public function created(Permission $hotel)
    {
        LogActivity::addToLog(__('log.add_data', [
            'user' => userForLog(),
            'data' => 'Permission '. $hotel->name,
        ]));
    }
    
    public function updated(Permission $hotel)
    {
        LogActivity::addToLog(__('log.edit_data', [
            'user' => userForLog(),
            'data' => 'Permission '. $hotel->name,
        ]));
    }

    public function deleted(Permission $hotel)
    {
        LogActivity::addToLog(__('log.delete_data', [
            'user' => userForLog(),
            'data' => 'Permission '. $hotel->name,
        ]));
    }
}
