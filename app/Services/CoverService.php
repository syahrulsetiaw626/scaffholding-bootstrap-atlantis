<?php 
    
namespace App\Service;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CoverService{

    public function uploadCover(Request $request, $module){
        try {
            $image = $request->file('cover');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $path =  'cover/'.$module.'/' . $imageName;
            $image->storeAs('cover/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]
            ];
        } catch (QueryException $e) {
            // return $e;
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }

    public function updateCover(Request $request, $module, $fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            $image = $request->file('cover');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $path =  'cover/'.$module.'/' . $imageName;
            $image->storeAs('cover/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]

            ];
        } catch (QueryException $e) {
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }

    public function deleteCover($fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            return true;
        } catch (QueryException $e) {
            return $e;
        }
    }
}