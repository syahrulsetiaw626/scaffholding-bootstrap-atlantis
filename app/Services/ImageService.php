<?php 
    
namespace App\Service;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageService{
    public function uploadImage(Request $request, $module){
        try {
            $image = $request->file('image');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $path =  'image/'.$module.'/' . $imageName;
            $image->storeAs('image/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]
            ];
        } catch (QueryException $e) {
            // return $e;
            return [
                'status' => false,
                'message' => $e
            ];
        }
        
    }

    public function updateImage(Request $request, $module, $fileBefore){
        
        try {
            Storage::disk('public')->delete($fileBefore);
            $image = $request->file('image');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $path =  'image/'.$module.'/' . $imageName;
            $image->storeAs('image/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]

            ];
        } catch (QueryException $e) {
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }
    
    public function deleteImage($fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            return true;
        } catch (QueryException $e) {
            return $e;
        }
    }
}