<?php 
    
namespace App\Service;

use getID3;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoService{
    public function storeVideo(Request $request){
        try {
            //input new video
            $video = $request->file('video');
            //call function getID3
            $getID3 = new getID3();
            //name video
            $videoName = date('YmdHis') . "." . $video->getClientOriginalExtension();
            //path video
            $path = 'video/' . $request->get('type_video') . "/". $request->get('title') . "/". $request->get('subtitle') . "/". $videoName;
            $video->storeAs('video/' . $request->get('type_video') . "/". $request->get('title') . "/". $request->get('subtitle') . "/", $videoName, ['disk' => 'public']);
            //duration video
            $file = $getID3->analyze($video);
            $duration = "00:" . $file['playtime_string'];
            //extension video
            $fileExt = $video->extension();
            //size video
            $fileSize = $video->getSize();

            //add function/logic to input and create
            $input['name'] = $videoName;
            $input['path'] = $path;
            $input['size'] = $fileSize;
            $input['extension'] = $fileExt;
            $input['duration'] = $duration;

            return [
                'status' => true,
                'message' => $path,
                'data' => $input
            ];
        } catch (QueryException $e) {
            return [
                'status' => true,
                'message' => $path,
                'data' => $input
            ];
        }   
        
    }

    public function updateVideo(Request $request, $id, $fileBefore){
        try {
            //delete photo first
            Storage::disk('public')->delete($fileBefore);
            //input new video
            $video = $request->file('video');
            //call function getID3
            $getID3 = new getID3();
            //name video
            $videoName = date('YmdHis') . "." . $video->getClientOriginalExtension();
            //path video
            $path = 'video/' . $request->get('type_video') . "/". $request->get('title') . "/". $request->get('subtitle') . "/". $videoName;
            $video->storeAs('video/' . $request->get('type_video') . "/". $request->get('title') . "/". $request->get('subtitle') . "/", $videoName, ['disk' => 'public']);
            //duration video
            $file = $getID3->analyze($video);
            $duration = "00:" . $file['playtime_string'];
            //extension video
            $fileExt = $video->extension();
            //size video
            $fileSize = $video->getSize();

            //add function/logic to input and create
            $input['name'] = $videoName;
            $input['path'] = $path;
            $input['size'] = $fileSize;
            $input['extension'] = $fileExt;
            $input['duration'] = $duration;

            return [
                'status' => true,
                'message' => $path,
                'data' => $input
            ];
        } catch (QueryException $e) {
            return [
                'status' => false,
                'message' => $e,
            ];
        }
        
    }

    public function deleteImage($fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            return true;
        } catch (QueryException $e) {
            return $e;
        }
    }
}