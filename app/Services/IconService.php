<?php 
    
namespace App\Service;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IconService{
    
    public function uploadIcon(Request $request, $module){
        try {
            $image = $request->file('icon');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $path =  'icon/'.$module.'/' . $imageName;
            $image->storeAs('icon/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]
            ];
        } catch (QueryException $e) {
            // return $e;
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }

    public function updateIcon(Request $request, $module, $fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            $image = $request->file('icon');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $path =  'icon/'.$module.'/' . $imageName;
            $image->storeAs('icon/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]

            ];
        } catch (QueryException $e) {
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }

    public function deleteIcon($fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            return true;
        } catch (QueryException $e) {
            return $e;
        }
    }
}