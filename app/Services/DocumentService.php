<?php 
    
namespace App\Service;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentService{

    public function uploadDocument(Request $request){
        try {
            $document = $request->file('document');
            $documentName = date('YmdHis') . "." . $document->getClientOriginalExtension();
            $path =  'document/'. $request->get('title'). '/' . $request->get('subtitle'). '/' . $documentName;
            $document->storeAs('document/'. $request->get('title'). '/' . $request->get('subtitle'). '/', $documentName, ['disk' => 'public']);
            $size = $document->getSize();
            $extension = $document->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $documentName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]
            ];
        } catch (QueryException $e) {
            // return $e;
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }

    public function updateDocument(Request $request, $fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            $document = $request->file('document');
            $documentName = date('YmdHis') . "." . $document->getClientOriginalExtension();
            $path =  'document/'. $request->get('title'). '/' . $request->get('subtitle'). '/' . $documentName;
            $document->storeAs('document/'. $request->get('title'). '/' . $request->get('subtitle'). '/', $documentName, ['disk' => 'public']);
            $size = $document->getSize();
            $extension = $document->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $documentName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]

            ];
        } catch (QueryException $e) {
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }
    
    public function deleteDocument($fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            return true;
        } catch (QueryException $e) {
            return $e;
        }
    }
}